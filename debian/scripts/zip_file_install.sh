#!/bin/bash

# This script will copy the zipfile from the zips/ folder (PACKAGE_ZIP_DIR) to
# debian/package/var/cache/package to have it included in the binary package.

set -e

SCRIPT_PATH=$(dirname "$0")

source "$SCRIPT_PATH/_components_to_package.sh"
source "$SCRIPT_PATH/_common_functions.sh"

for version in ${PLATFORMS_VERSIONS_TO_PACKAGE} \
  ${BUILD_TOOLS_VERSIONS_TO_PACKAGE} \
  ${PATCHER_VERSIONS_TO_PACKAGE} \
  ${CMDLINE_TOOLS_VERSIONS_TO_PACKAGE} \
  ${PLATFORM_TOOLS_VERSIONS_TO_PACKAGE} \
  ${NDK_VERSIONS_TO_PACKAGE} \
  ${SOURCES_VERSIONS_TO_PACKAGE} \
  ${EMULATOR_VERSIONS_TO_PACKAGE} \
  ${EXTRAS_GOOGLE_AUTO_VERSIONS_TO_PACKAGE} \
  ${TOOLS_VERSIONS_TO_PACKAGE} \
  ${SDK_DOCS_VERSIONS_TO_PACKAGE}; do

  ZIPFILE="$(get_zip_filename "$version")"
  PKG_NAME="$(get_package_name "$version")"

  if [ -e "$PACKAGE_ZIP_DIR/$ZIPFILE" ]; then
    echo "    Installing zip file of $version"
    cp --reflink=auto -a "$PACKAGE_ZIP_DIR/$ZIPFILE" "debian/$PKG_NAME/var/cache/$PKG_NAME/$ZIPFILE.in"
  fi
done
