#!/bin/bash

set -e

# Get the binary size of unknown packages by downloading them
# Use this script in a SCHROOT environment to not polute yours
# as it downloads and removes the packages for which size is unknown

# Usage:
#   Run this from a schroot environment
#     ./debian/scripts/get_unpacked_size.sh
#

SCRIPT_PATH=$(dirname "$0")

source "$SCRIPT_PATH/_components_to_package.sh"
source "$SCRIPT_PATH/_common_functions.sh"

if [ -z "$debian_chroot" ] || [ $UID -ne 0 ]; then
  echo "This must be run in a schroot as user root."
  exit
fi

check_empty_android-sdk_dir() {
  if [ -e /usr/lib/android-sdk ]; then
    echo "First remove all google-android-* packages to have no /usr/lib/android-sdk dir"
    exit
  fi
}

get_unpacked_size() {
  # $1 = $ZIPFILE
  # $2 = $PKG_NAME
  # $3 = $PATH

  if ! grep --perl-regex -q "^$1\t" ../version_list_unpacked_size.txt; then
    # Installs the deb package to download the archive on google servers
    # Then gets the uncompressed size of the downloaded archive
    # Finally purge the package
    check_empty_android-sdk_dir
    dpkg \
      --ignore-depends="zlib1g" \
      --ignore-depends="libstdc++6" \
      --ignore-depends="lib32z1" \
      --ignore-depends="lib32stdc++6" \
      --ignore-depends="default-jre-headless" \
      --ignore-depends="google-android-licenses" \
      --ignore-depends="google-android-build-tools" \
      --ignore-depends="lib32gcc-s1" \
      --ignore-depends="lib32gcc1" \
      --ignore-depends="libc6-i386" \
      --ignore-depends="libswt-gtk-3-java" \
      --ignore-depends="openjdk-8-jre-headless" \
      --ignore-depends="fonts-roboto" \
      -i ../../../"${2}"_*.deb
    SIZE=$(du -b "${3}" | tail -1 | cut -d "	" -f1)
    dpkg -P "${2}"
    echo -e "$1\t$SIZE" >> ../version_list_unpacked_size.txt
  else
    UNPACKED_SIZE=$(grep --perl-regex "^$1\t" ../version_list_unpacked_size.txt | cut -d "	" -f2 | sed "s/^$/0/")
    echo "Size for $1 already known: $UNPACKED_SIZE"
  fi
}

SCRIPT_PATH=$(dirname "$0")

cd "$SCRIPT_PATH"
source _components_to_package.sh

if ! command -v inotifywait; then
  echo "inotifywait is missing. Install the package inotify-tools first."
  exit 1
fi

inotifywait -e moved_to --include 'google-.*' -r -m /var/cache/ |
  while read -r dir action file; do
    if [[ $file =~ \.zip$ ]]; then
      echo "The file '$file' appeared in directory '$dir' via '$action'"
      # Save a copy of zip file to the parent dir of the repo.
      cp -a "$dir/$file" "${SCRIPT_PATH}"/../../../
    fi
  done &

for version in ${PLATFORMS_VERSIONS_TO_PACKAGE} \
  ${BUILD_TOOLS_VERSIONS_TO_PACKAGE} \
  ${PATCHER_VERSIONS_TO_PACKAGE} \
  ${CMDLINE_TOOLS_VERSIONS_TO_PACKAGE} \
  ${PLATFORM_TOOLS_VERSIONS_TO_PACKAGE} \
  ${NDK_VERSIONS_TO_PACKAGE} \
  ${SOURCES_VERSIONS_TO_PACKAGE} \
  ${EMULATOR_VERSIONS_TO_PACKAGE} \
  ${EXTRAS_GOOGLE_AUTO_VERSIONS_TO_PACKAGE} \
  ${TOOLS_VERSIONS_TO_PACKAGE}; do

  get_unpacked_size "$(get_zip_filename "$version")" "$(get_package_name "$version")" "/usr/lib/android-sdk/$(get_path "$version")"
done

for version in ${SDK_DOCS_VERSIONS_TO_PACKAGE}; do
  get_unpacked_size "$(get_zip_filename "$version")" "$(get_package_name "$version")" "/usr/share/doc/google-android-sdk-docs/"
done

# Kill all subprocess (to kill inotifywait)
kill -- -$$
