#!/bin/bash

# This script will prepare a directory containg a source package for the Android component
# that ships google's binaries
#
# ./debian/scripts/make_zip_package.sh ndk,25.1.8937393 build-tools,33.0.0 platforms,android-33~33 platform-tools
#
# Env vars:
#  - EPOCH : override epoch
#  - VER : override version
#  - REV : override revision
#  - SKIP_SIGN : if set to 1, don't sign the package
#  - SPLIT : if non empty, each binary package will have a corresponding source package of same name.
#

set -e

SCRIPT_PATH=$(dirname "$0")
source "$SCRIPT_PATH"/_components_to_package.sh
source "$SCRIPT_PATH"/_common_functions.sh

CWD="$(realpath "$SCRIPT_PATH")"

LOCAL_ZIP_DIR="$(realpath ../google-android-installers-zips)"

TMPDIR=$(mktemp -d /tmp/gai-zip-XXX) || exit
if [ ! a"$DEBUG" = a"1" ]; then
  # shellcheck disable=SC2064
  trap "echo removing $TMPDIR && rm -rf -- '$TMPDIR'" EXIT
else
  # shellcheck disable=SC2064
  trap "echo Files are in: $TMPDIR" EXIT
fi

ORIG_PKG_NAME=$(dpkg-parsechangelog -S Source)
ORIG_DEB_VERSION_UPSTREAM_REVISION=$(dpkg-parsechangelog -S Version | cut -f 2 -d ":")
ORIG_DEB_VERSION_UPSTREAM=$(cut -f 1 -d "-" <<< "$ORIG_DEB_VERSION_UPSTREAM_REVISION")
ORIG_EPOCH=$(dpkg-parsechangelog -S Version | cut -f 1 -d ":" -s)
# SOURCE_DATE_EPOCH="$(dpkg-parsechangelog -STimestamp)"

PKGDIR="$TMPDIR/$ORIG_PKG_NAME"

build() {
  if [ "a$SKIP_SIGN" = "a1" ]; then
    dpkg-buildpackage -S --no-sign
  else
    dpkg-buildpackage -S
  fi
  local deb_version deb_name
  deb_version="$(dpkg-parsechangelog -S Version | cut -f 2 -d ":")"
  deb_name="$(dpkg-parsechangelog -S Source)"
  echo "${deb_name}_${deb_version}_source.changes" >> ../generated_changes.txt
  echo "${deb_name}_${deb_version}.dsc" >> ../generated_dsc.txt
}

make_zip_orig_tar() {
  local pkg_name version package_zip_dir SOURCE_DATE_EPOCH
  pkg_name="$1"
  version="$2"
  package_zip_dir="$3"

  # Always set source_date_epoch to 2015-01-01, so that orig.tar is always identical whatever the DEB_VERSION.
  SOURCE_DATE_EPOCH="1420066800"

  tar --sort=name \
    --mtime="@${SOURCE_DATE_EPOCH}" \
    --owner=0 --group=0 --numeric-owner \
    --pax-option=exthdr.name=%d/PaxHeaders/%f,delete=atime,delete=ctime \
    -cJf "../${pkg_name}_${version}.orig-$package_zip_dir.tar.xz" "$package_zip_dir"
}

copy_orig_tar() {
  local orig_pkg_name orig_version pkg_name version new_name
  orig_pkg_name="$1"
  orig_version="$2"
  pkg_name="$3"
  version="$4"

  if [ "$orig_pkg_name" = "$pkg_name" ] && [ "$orig_version" = "$version" ]; then return; fi

  cd ..
  while read -r line; do
    # shellcheck disable=SC2001
    new_name="$(echo "$line" | sed "s/${orig_pkg_name}_${orig_version}/${pkg_name}_${version}/")"
    cp -a "$line" "$new_name"
  done < <(find . -maxdepth 1 -name "$orig_pkg_name*orig*.tar.xz")
  cd -
}

mkdir -p "$PKGDIR"

cp -a . "$PKGDIR"
cp -av ../"${ORIG_PKG_NAME}_${ORIG_DEB_VERSION_UPSTREAM}.orig"*xz "$TMPDIR"
rm -fr "$PKGDIR"/.git

pushd "$PKGDIR"

# Set version of package
if [ ! "a$VER" = "a" ]; then
  VERSION="$VER"
else
  VERSION="$ORIG_DEB_VERSION_UPSTREAM"
fi
if [ ! "a$REV" = "a" ]; then
  REVISION="$REV"
else
  REVISION="$(dpkg-parsechangelog -S Version | cut -f 2 -d "-" -s)"
fi
if [ -z "$EPOCH" ]; then EPOCH=$ORIG_EPOCH; fi
if [ ! "a$EPOCH" = "a" ]; then
  if [ -z "$REVISION" ]; then
    sed -i "1 s/(\(.*\))/($EPOCH:$VERSION)/" debian/changelog
  else
    sed -i "1 s/(\(.*\))/($EPOCH:$VERSION-$REVISION)/" debian/changelog
  fi
else
  if [ -z "$REVISION" ]; then
    sed -i "1 s/(\(.*\))/($VERSION)/" debian/changelog
  else
    sed -i "1 s/(\(.*\))/($VERSION-$REVISION)/" debian/changelog
  fi
fi

# Set XS-With-Zips: yes in d/control
sed -i "/XS-Autobuild/a XS-With-Zips: yes" debian/control.in

if [ ! "a$SPLIT" = "a" ]; then
  # Create individual source package for each requested component
  cp -a debian/control.in ..

  for item in "$@" google-android-licenses; do
    # Select only one package

    rm -f "debian/version_list.txt.single"
    debian/scripts/build_version_list.sh

    if [ "$item" = "google-android-licenses" ]; then
      cp /dev/null debian/version_list.txt.single
      cp -a ../control.in debian/control.in
      PKG_NAME="google-android-licenses"
      sed -i "1 s/.* \(.*)\)/$PKG_NAME \1/" debian/changelog
      sed -i "/^Source:/ s/\(.*: \).*/\1$PKG_NAME/" debian/control.in
      # shellcheck disable=SC2153
      rm -fr "$PACKAGE_ZIP_DIR"
    else
      get_line "$item" > debian/version_list.txt.single

      # Update package source name to binary name
      PKG_NAME=$(get_package_name "$item")
      sed -i "1 s/.* \(.*)\)/$PKG_NAME \1/" debian/changelog
      sed -n "/^Package: google-android-licenses/q;p" ../control.in > debian/control.in
      sed -i "/^Source:/ s/\(.*: \).*/\1$PKG_NAME/" debian/control.in

      rm -fr "$PACKAGE_ZIP_DIR"
      debian/scripts/zip_file_add.sh "${LOCAL_ZIP_DIR}" "$item"
    fi

    debian/scripts/build_control_file.sh

    if [ ! "$item" = "google-android-licenses" ]; then
      make_zip_orig_tar "${PKG_NAME}" "${VERSION}" "$PACKAGE_ZIP_DIR"
    fi

    copy_orig_tar "${ORIG_PKG_NAME}" "${ORIG_DEB_VERSION_UPSTREAM}" "${PKG_NAME}" "${VERSION}"

    build
  done

else
  debian/scripts/zip_file_add.sh "${LOCAL_ZIP_DIR}" "$@"

  make_zip_orig_tar "${ORIG_PKG_NAME}" "${ORIG_DEB_VERSION_UPSTREAM}" "$PACKAGE_ZIP_DIR"

  copy_orig_tar "${ORIG_PKG_NAME}" "${ORIG_DEB_VERSION_UPSTREAM}" "${ORIG_PKG_NAME}" "${VERSION}"

  build
fi

popd

DESTDIR="$(mktemp -d "$CWD/../../../$ORIG_PKG_NAME-with-zips.XXX")"
dcmd cp -a -v "$TMPDIR"/*.dsc "$DESTDIR"
dcmd cp -a -v "$TMPDIR"/*_source.changes "$DESTDIR"
cp -a "$TMPDIR"/generated_dsc.txt "$DESTDIR"
cp -a "$TMPDIR"/generated_changes.txt "$DESTDIR"
